/* 
 * File:   Edge.h
 * Author: rudolph
 *
 * Created on 28 Ноябрь 2013 г., 22:14
 */

#ifndef EDGE_H
#define	EDGE_H

class Edge {

private:    
    int vert1;
    int vert2;
    int weight;
    
public:
    Edge(int v1, int v2, int w ){
        vert1 = v1;
        vert2 = v2;
        weight = w;
    }
    
    int getFirstVert(){
       return vert1; 
    }
    
    int getSecondVert(){
        return vert2;
    }
    
    int getWeight(){
        return weight;
    }
};

#endif	/* EDGE_H */

