/* 
 * File:   Graph.h
 * Author: rudolph
 *
 * Created on 27 Ноябрь 2013 г., 23:34
 */
#include <vector>
#include <fstream>
using namespace std;


#ifndef GRAPH_H
#define	GRAPH_H


typedef vector<int> adjVerts;

class Graph {
public:
    Graph();
    Graph(const Graph& orig);
    virtual ~Graph();
    Graph(int n);
    Graph(fstream &file,const char* filename);
            
    void setWeight(int from, int to, int weight);
    int getWeight(int from, int to);
    void print();
    int getSizeRows();
    int getSizeCols();
    
public:
    static const int INF = 1000000;
    int N;      // количество вершин
private:
    vector<adjVerts> verts;
    
    

};

#endif	/* GRAPH_H */

