/* 
 * File:   parallel.h
 * Author: rudolph
 *
 * Created on 15 Декабрь 2013 г., 20:22
 */

#include "Graph.h"

#ifndef PARALLEL_H
#define	PARALLEL_H

void parallelPrim();

void findMinEdge(int currentThreadNum, int totalThreads);

int findMinEdgeInFuture(int currentThreadNum, int totalThreads);
void futurePrim(int numThreads);

    
    
    
    
    
    


#endif	/* PARALLEL_H */

