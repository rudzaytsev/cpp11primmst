#include "parallel.h"
#include "MST.h"

#include <vector>
#include <thread>
#include <mutex>
#include <iostream>
#include <future>
#define DATA_FILE_NAME "test02.txt"
#define N_THREADS 2

using namespace std;

 fstream file;
 Graph graph(file,DATA_FILE_NAME);
 int n = graph.N;
    
 vector<bool> inMst (n);
 vector<int> minEdge (n, Graph::INF);
 vector<int> parrent (n, -1);
 
 vector<int> foundCandidateVerts;
 
 mutex some_mutex;
 

void findMinEdge(int currentThreadNum, int totalThreads){
    
    int totalVerts = graph.N;
    int start = (currentThreadNum - 1) * (totalVerts / totalThreads);
    int finish = currentThreadNum * (totalVerts / totalThreads);
    
   // cout << " totalThreads = " << totalThreads << endl;
   // cout << " start  = " << start << endl;
      
    if(currentThreadNum == totalThreads){
        finish = totalVerts;
    }
    
    //cout << " finish = " << finish << endl;
    
    int v = -1;
    for (int j = start; j < finish; ++j){

            if (!inMst[j] &&
                (v == -1 || minEdge[j] < minEdge[v])){
                    v = j;
            }
    }
 
    lock_guard<mutex> guard(some_mutex);
    foundCandidateVerts.push_back(v);
    
    
} 
 


void parallelPrim(){
    
     minEdge[0] = 0;
     
     thread myThreads[N_THREADS];
     
     
     for(int k = 0; k < graph.N; ++k){
                
        for(int j = 0; j < N_THREADS; j++){
            
            myThreads[j] = thread(findMinEdge,j+1,N_THREADS);
        }
 
        for(int i = 0; i < N_THREADS; i++){
            
            myThreads[i].join();            
        }
        
        
        int minE = graph.INF;
        int vert  = -1; 
    
        for(int i = 0; i < N_THREADS; i++){
            if(foundCandidateVerts.at(i) < 0){
                continue;
            }
            if(minEdge[foundCandidateVerts.at(i)] < minE){
                minE = minEdge[foundCandidateVerts.at(i)];
                vert = foundCandidateVerts.at(i);
            }
        }
    
        if(vert < 0){ 
            cout << "Vert Error! No MST!!!" << endl;
            return;
        }
    
        inMst[vert] = true;
    
        //cout << "Added in Mst vert = " << vert << endl;
         for (int to = 0; to < graph.N; ++to){
               // cout << "INSIDE 1" << endl;
               // cout << " v = " << v << " to= " << to << endl;
               // cout << "rows size = " << g.getSizeRows() << endl;
               // cout << "cols size = " << g.getSizeCols() << endl;
             if (graph.getWeight(vert,to) < minEdge[to]) {
                 //cout << "INSIDE 2" << endl;
                 minEdge[to] = graph.getWeight(vert,to);
                 parrent[to] = vert;
             }
         }
        
        foundCandidateVerts.clear();
                
        
    }
    
   cout << "mst weight = " << calcWeightMST(parrent, graph) << endl;
  // printMstEdges(parrent);
     
        
} 
 

 
int findMinEdgeInFuture(int currentThreadNum, int totalThreads){
    
    int totalVerts = graph.N;
    int start = (currentThreadNum - 1) * (totalVerts / totalThreads);
    int finish = currentThreadNum * (totalVerts / totalThreads);
    
    cout << " totalThreads = " << totalThreads << endl;
    cout << " start  = " << start << endl;
   
    
    if(currentThreadNum == totalThreads){
        finish = totalVerts;
    }
    
     cout << " finish = " << finish << endl;
    
    int v = -1;
    for (int j = start; j < finish; ++j){

            if (!inMst[j] &&
                (v == -1 || minEdge[j] < minEdge[v])){
                    v = j;
            }
    }
 
    return v;
    
    
    
}


void futurePrim( int numThreads){
    
   minEdge[0] = 0; 
   vector<int> foundVerts;
       
    for(int k = 0; k < graph.N; ++k) {
        
	future<int> *futureVert = new future<int>[numThreads];
        cout << "MY STR " << endl;
        for(int j = 0; j < numThreads; j++){
                cout << "ITER " << j << endl;
                try{
                        futureVert[j] = async(findMinEdgeInFuture,j+1,numThreads);
                        futureVert[j].wait();
                        int val = futureVert[j].get();
                        cout << "VAL = " << val << endl;
                        foundVerts.push_back(val);
                        
                }
                catch(exception &e){
                    cout << e.what() << endl;
                }
        }
 
        for(int i = 0; i < numThreads; i++){
            
           
            //foundVerts.push_back(val);
            
        }
        
        
        int minE = graph.INF;
        int vert  = -1; 
    
        for(int i = 0; i < numThreads; i++){
            if(foundVerts.at(i) < 0){
                continue;
            }
            if(minEdge[foundVerts.at(i)] < minE){
                minE = minEdge[foundVerts.at(i)];
                vert = foundVerts.at(i);
            }
        }
    
        if(vert < 0){ 
            cout << "Vert Error! No MST!!!" << endl;
            return;
        }
    
        inMst[vert] = true;
    
        cout << "Added in Mst vert = " << vert << endl;
         for (int to = 0; to < graph.N; ++to){
               // cout << "INSIDE 1" << endl;
               // cout << " v = " << v << " to= " << to << endl;
               // cout << "rows size = " << g.getSizeRows() << endl;
               // cout << "cols size = " << g.getSizeCols() << endl;
             if (graph.getWeight(vert,to) < minEdge[to]) {
                 //cout << "INSIDE 2" << endl;
                 minEdge[to] = graph.getWeight(vert,to);
                 parrent[to] = vert;
             }
         }
        
        foundVerts.clear();
        
        //delete [] futureVert;
        
    }
    
   cout << "mst weight = " << calcWeightMST(parrent, graph) << endl;
   printMstEdges(parrent);
    
        
}




