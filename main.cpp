/* 
 * File:   main.cpp
 * Author: rudolph
 *
 * Created on 15 Декабрь 2013 г., 16:02
 */

#include <iostream>
#include <thread>
#include <time.h>
#include "parallel.h"
#include "MST.h"

using namespace std;

extern Graph graph;


void hello(){
    
    std::cout << "Hello parellel world" << std::endl;
    std::cout << std::thread::hardware_concurrency() << std::endl;
}
 
 
int main() {
    /*
    try{
        std::thread t(hello);
        t.join();
    }
    catch (std::exception &e){
        std :: cout << e.what() << std :: endl;
    }
    */
    
    clock_t time;
    time = clock();
    
    parallelPrim();
    
    time = clock() - time;
    cout <<  "*********** PARALLEL PRIM *************" << endl;
    cout << "PARALELL PRIM TIME = " << ((double) time ) / CLOCKS_PER_SEC << " seconds" << endl;
    cout << "PARALELL PRIM TIME = " << ((double) time ) << " ticks" << endl;
    
    clock_t time2;
    time2 = clock();
    prim(graph,graph.N );
            
    time2 = clock() - time2;
    cout <<  "*********** SERIAL PRIM *************" << endl;
    cout << "SERIAL PRIM TIME = " << ((double) time2 ) / CLOCKS_PER_SEC << " seconds" << endl;
    cout << "SERIAL PRIM TIME = " << ((double) time2 )  << " ticks" << endl;

      
    return 0;
}

